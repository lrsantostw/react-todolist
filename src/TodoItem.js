import React, {Component} from 'react';
import './style.css'

class TodoItem extends Component {
    constructor(props){
        super(props)

        this.state = this.props.item
    }
    
    changeChecked = () => {
        this.setState((prevState) => {
            prevState.checked = !prevState.checked
            return prevState
        })
    }

    render(){
        const completedStyle = {
            fontStyle: "italic",
            color: "#cdcdcd",
            textDecoration: "line-through"
        }
        const invisible = {
            display: "none"
        }

        return(
            <div className='todo-item'>
                <input type='checkbox' checked={this.state.checked} onChange={this.changeChecked}/>
                <p id= "item-text" style= {this.state.checked ? completedStyle : null}>{this.state.task}</p>
                
                <button style={this.state.checked ? null : invisible} id="delButton" type="button" onClick={() => this.props.action(this.state.id)}>X</button>
                 
            </div>
        );
    }
}

export default TodoItem;