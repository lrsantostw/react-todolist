import React from 'react';

import TodoItem from './TodoItem';
import todoData from './todoData';
import './App.css';
import './style.css';

class App extends React.Component{
  constructor(props){
    super(props)

    this.state = {data: todoData,
                  lastkey: 5};
  }
  
  addTask = (id) => {
    let newTask = document.getElementById(id).value
    document.getElementById(id).value = ""
    
    this.setState((prevState) => {
      prevState.lastkey = prevState.lastkey + 1

      prevState.data.push(  {id: prevState.lastkey,
                            task: newTask,
                            checked: false})
      return prevState
    })
  }

  deleteTask = (id) => {
    this.setState(prevState => {
      delete prevState.data[id-1]
      return prevState
    })
  }

  addTaskFromEnter = (target,id) => {
    if(target.key === "Enter"){
      this.addTask(id)
    }
  }

  render(){
    let itens = this.state.data.map(item => <div>
                                              <TodoItem key={item.id} item={item} action={this.deleteTask}/>
                                            </div>);

    return (
      <div className="todo-list">
        <div> 
          <input type="text" name="item-to-add" id="newTask"
                 onKeyPress={target => this.addTaskFromEnter(target,"newTask")}/>
          <button id="addButton" type="button" 
                  onClick={() => this.addTask("newTask")}>Add task</button>
        </div>

        {itens}
      </div>
    );
  }
}

export default App;

